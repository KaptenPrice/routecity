class Constants {

    static final int NUMBER_OF_VERTICES = 10;
    static final int CAPACITY = 10;
    static String[] NAMES = {"Bellevue", "SKF", "Gamlestaden", "Centralstationen", "Redbergsplatsen", "Brunnsparken", "Kungsportsplatsen", "Svingeln", "Olskroken", "Domkyrkan"};

}

