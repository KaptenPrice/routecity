import org.junit.jupiter.api.*;

public class GraphTests {

    @Test
    void testAddEdge() {
        Graph testGraph = new Graph();
        testGraph.addEdge(0, 1, 9);
        int acutalSource = testGraph.adjacentVerticesList[0].get(0).source;
        int actualWeight = testGraph.adjacentVerticesList[0].get(0).weight;
        int acutalDestination = testGraph.adjacentVerticesList[0].get(0).destination;
        int expectedSource = 0;
        int expectedDestination = 1;
        int expectedW = 9;
        Assertions.assertEquals(expectedSource, acutalSource);
        Assertions.assertEquals(expectedDestination, acutalDestination);
        Assertions.assertEquals(expectedW, actualWeight);
    }
    @Test
    void testGraphIsUndirected() {
        Graph testGraph = new Graph();
        testGraph.addEdge(0, 1, 9);
        int acutalSource = testGraph.adjacentVerticesList[1].get(0).source;
        int actualWeight = testGraph.adjacentVerticesList[1].get(0).weight;
        int acutalDestination = testGraph.adjacentVerticesList[1].get(0).destination;
        int expectedSource = 1;
        int expectedDestination = 0;
        int expectedW = 9;
        Assertions.assertEquals(expectedSource, acutalSource);
        Assertions.assertEquals(expectedDestination, acutalDestination);
        Assertions.assertEquals(expectedW, actualWeight);
    }

    @Test
    void dijkstraGetMinDistance() {
        Graph graph = new Graph();
       // graph.dijkstraGetMinDistance(0);

        //Assertions.assertEquals(expected, actual);
    }

    @Test
    void decreaseKey() {
    }
   /* @Test
    void testIfEmptyList(){
        Graph testGraph=new Graph(2);
        testGraph.addEdge(1,0,9);
        testGraph.addEdge(2,0,9);
        testGraph.addEdge(3,0,9);
        int acutalSource = testGraph.adjacentVertices[0].get(0).source;
        int actualWeight = testGraph.adjacentVertices[0].get(0).weight;
        int acutalDestination = testGraph.adjacentVertices[0].get(0).destination;
        int expectedSource = 1;
        int expectedDestination = 0;
        int expectedW = 9;
        Assertions.assertEquals(expectedSource, acutalSource);
        Assertions.assertEquals(expectedDestination, acutalDestination);
        Assertions.assertEquals(expectedW, actualWeight);
    }*/
}
